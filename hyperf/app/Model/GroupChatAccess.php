<?php

declare (strict_types=1);
namespace App\Model;

/**
 * @property int $id 
 * @property int $gid 
 * @property int $uid 
 * @property int $dateline 
 */
class GroupChatAccess extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'group_chat_access';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['id' => 'integer', 'gid' => 'integer', 'uid' => 'integer', 'dateline' => 'integer'];
}