<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use Hyperf\Di\Annotation\Inject;
use App\Model\User;
use App\Model\GroupChat;
use App\Model\GroupChatMsg;
use App\Model\TmpMessage;
use App\Logic\Aes;


class ApiController extends AbstractController
{

    /**
     * @Inject()
     * @var \Hyperf\Contract\SessionInterface
     */
    protected $session;

    /**
     * @Inject()
     * @var \Hyperf\HttpServer\Contract\RequestInterface
     */
    protected $request;

    public function setUser(){
        $userName = $this->request->input('username', false);
        if ($userName === false) {
            return ['code'=>'500', 'msg'=>'请输入您的用户名'];
        }
        $user = User::query()->where('username', $userName)->first();
        if(!$user->username){
            $user = new User();
            $user->username = $userName;
            $user->dateline = time();
            $uid = $user->save();
        }
        $this->session->set('uid', $user->id);
        $this->session->set('username', $user->username);
        return ['code'=>'200', 'msg'=>'帐号验证成功', 'url'=>'/groupChat'];
    }
    
    public function getUserInfo(){
        if($this->session->get('uid') && $this->session->get('username')){
            return ['code'=>200, 'msg'=>'获取用户信息成功', 'uid'=>$this->session->get('uid'), 'username'=>$this->session->get('username')];
        }
        return ['code'=>'500', 'msg'=>'用户信息获取失败，您的信息不完整！'];
    }
    
    public function getGroupChat(){
        $groupChat = GroupChat::query()->find('2');
        if(!$groupChat){
            $groupChat = new GroupChat();
            $groupChat->title = '自动创建的测试服务器';
            $groupChat->dateline = time();
            $groupChat->save();
        }
        return ['code'=>200, 'msg'=>'获取群聊信息成功', 'gid'=>$groupChat->id, 'gname'=>$groupChat->title, 'ws'=>'ws://193.112.106.192:9502'];
    }
    
    public function getHistoryMessage(){
        if($this->request->input('gid', false) === false){
            return ['code'=>500, 'msg'=>'参数错误'];
        }
        $mgsList = GroupChatMsg::query()->where('gid', $this->request->input('gid'))->orderBy('dateline', 'desc')->limit(30)->get();
        $responseData = [];
        foreach ($mgsList as $key => $value){
            array_unshift($responseData, [
                'author'        =>  $value->username,
                'dateline'      =>  date('Y-m-d H:i:s', $value->dateline),
                'message'       =>  htmlspecialchars(htmlspecialchars_decode($value->message))
            ]);
        }
        return ['code'=>200, 'msg'=>'获取历史消息成功', 'listMessage'=>$responseData];
    }
    
    public function getMessageToken(){
        if(!$this->session->get('uid')){
            return ['code'=>500, 'msg'=>'您还未进行帐号登陆'];
        }else if($this->request->input('message', false) === false){
            return ['code'=>500, 'msg'=>'消息不能为空'];
        }
        $groupChat = GroupChat::query()->find($this->request->input('gid'));
        if (!$groupChat && $this->request->input('gid')) {
            return ['code'=>500, 'msg'=>'聊天室错误'];
        }
        $tmpMessage = new TmpMessage();
        $tmpMessage->uid = $this->session->get('uid');
        $tmpMessage->username = $this->session->get('username');
        if($this->request->input('gid')){
            $tmpMessage->gid = $groupChat->id;
        }
        $tmpMessage->message = htmlspecialchars($this->request->input('message'));
        $tmpMessage->save();
        $tmpid = Aes::encrypt($tmpMessage->id);
        return ['code'=>'200', 'msg'=>'success', 'token'=>$tmpid];
    }
}
