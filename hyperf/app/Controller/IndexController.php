<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use Hyperf\Di\Annotation\Inject;

class IndexController extends AbstractController
{

    /**
     * @Inject()
     * @var \Hyperf\Contract\SessionInterface
     */
    protected $session;

    /**
     * @Inject()
     * @var \Hyperf\View\RenderInterface
     */
    protected $render;

    /**
     * @Inject()
     * @var \Hyperf\HttpServer\Contract\ResponseInterface
     */
    protected $response;
    
    public function index()
    {
        return $this->response->redirect('/login');
        
        $user = $this->request->input('user', 'Hyperf');
        $method = $this->request->getMethod();

        return [
            'method' => $method,
            'message' => "Hello {$user}.",
        ];
    }
    
    
    public function login(){
        if ($this->session->get('uid')) {
            return $this->response->redirect('/groupChat');
        }
        return $this->render->render('login');
    }
    
    public function groupChat(){
        if (!$this->session->get('uid')) {
            return $this->response->redirect('/login');
        }
        return $this->render->render('groupChat');
    }
}
