<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
use Hyperf\HttpServer\Router\Router;

Router::addRoute(['GET', 'POST', 'HEAD'], '/', 'App\Controller\IndexController@index');

Router::get('/favicon.ico', function () {
    return '';
});

Router::addServer('ws', function () {
    Router::get('/', 'App\Controller\WebSocketController');
});

Router::get('/login', 'App\Controller\IndexController@login');
Router::get('/groupChat', 'App\Controller\IndexController@groupChat');


Router::post('/login', 'App\Controller\ApiController@setUser');
Router::post('/getUserInfo', 'App\Controller\ApiController@getUserInfo');
Router::post('/getGroupChat', 'App\Controller\ApiController@getGroupChat');
Router::post('/getHistoryMessage', 'App\Controller\ApiController@getHistoryMessage');
Router::post('/getMessageToken', 'App\Controller\ApiController@getMessageToken');
