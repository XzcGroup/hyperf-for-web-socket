# hyperfForWebSocket

#### 介绍
目前项目支持功能有：创建帐号 登录帐号 多人群聊（即时通讯）上线通知 下线通知 XSS防范 聊天记录保存 聊天记录展示

#### 软件架构
服务器环境：PHP7.4 、 Mysql8.0 、 Redis
软件架构：Swoole的Hyperf框架 采用框架中WebSocket服务和Http服务。前端采用LayUI前端开源包


#### 安装教程

1.  克隆代码
2.  部署代码到网站根目录
3.  导入数据库数据
4.  修改软件配置
5.  命令行执行“php dir/bin/hyperf start”启动HTTP服务以及WebSocket服务 dir为网站根目录

#### 使用说明

1.  默认HTTP端口为9501
2.  默认WebSocket端口为9502

#### 效果截图
![Image text](https://gitee.com/XzcGroup/hyperf-for-web-socket/raw/master/WechatIMG340.png)
